// +build linux

package main

/*
#cgo LDFLAGS: -ldl
#include <dlfcn.h>
#include <stddef.h>

void *handle;
int (*mos_start)(int,char**);

int init_mos() {
  char *error;
  handle = dlopen("./mosquitto.so", RTLD_NOW);
  if (!handle) {
    return 1;
  }
  mos_start = dlsym(handle, "mos_main");
  if ((error = dlerror()) != NULL) {
    return 2;
  }
	return 0;
}

int run_mos(int argc,char** argv) {
	int rv = (*mos_start)(argc, argv);
	dlclose(handle);
	return rv;
}

*/
import "C"

import (
	"fmt"
	"log"
)

// Mosquitto 模块
type Mosquitto struct {
	config string
}

var mosquitto *Mosquitto

func initMosquitto(conf mosquittoConfig) (m *Mosquitto, err error) {
	m = &Mosquitto{conf.Config}
	rv := C.init_mos()
	switch int(rv) {
	case 0:
		log.Println("mosquitto模块成功初始化.")
	case 1:
		return nil, fmt.Errorf("无法加载mosquitto动态库，检查是否缺少某些依赖例如uuid-dev，以及是否将mosquitto.so放在正确目录")
	case 2:
		return nil, fmt.Errorf("无法解析mosquitto动态库")
	}
	mosquitto = m
	return
}

// Run 启动Mosquitto模块
func (m *Mosquitto) Run() error {
	argc := C.int(3)
	argv := make([]*C.char, 3)
	argv[0] = C.CString("mosquitto")
	argv[1] = C.CString("-c")
	argv[2] = C.CString(m.config)
	rv := C.run_mos(argc, &argv[0])
	if int(rv) != 0 {
		return fmt.Errorf("mosquitto模块异常退出: %d", int(rv))
	}
	return nil
}
