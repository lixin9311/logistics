package main

const (
	inboundStr  = "inbound"
	outboundStr = "outbound"
	mainStorage = "main_storage"
	subStorage  = "sub_storage"
	vehicle     = "vehicle"
	cloud       = "cloud"
)

// Message 是所用通信报文的核心
type Message struct {
	Records   []Record    `json:"Records"`
	TimeStamp int64       `json:"TimeStamp,omitempty"`
	Extension interface{} `json:"Extension,omitempty"`
}

// Record 是一条记录，包括了物品的ID和数量
type Record struct {
	ID       string
	Quantity int
}
