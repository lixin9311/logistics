package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/emicklei/go-restful"
)

type status struct {
	Code   int
	Reason string `json:"Reason,omitempty"`
}

func runSubStorage(conf tomlConfig) {

	if _, err := initMqtt(conf.MQTT); err != nil {
		log.Fatalln("初始化MQTT客户端出错: ", err)
	}
	if err := mqttclient.Run(); err != nil {
		log.Fatalln("启动MQTT主业务出错: ", err)
	}
	if err := subHTTPInit(conf.APIPort); err != nil {
		log.Fatalln("启动HTTP借口服务出错: ", err)
	}
}

func subHTTPInit(port int) error {
	wsContainer := restful.NewContainer()
	userWS := new(restful.WebService)
	userWS.
		Path("/data").
		Consumes(restful.MIME_JSON).
		Produces(restful.MIME_JSON)

	userWS.Route(userWS.GET("/{VehicleID}/{Bound}/{CargoID}").
		To(subDataHandler).
		Doc("get a access token").
		Operation("getAccessToken").
		Writes(status{}).
		Param(userWS.PathParameter("VehicleID", "VehicleID").DataType("string")).
		Param(userWS.PathParameter("Bound", "Bound").DataType("string")).
		Param(userWS.PathParameter("CargoID", "CargoID").DataType("string")).
		Param(userWS.QueryParameter("Quantity", "Quantity").DataType("int")).
		Returns(200, "OK", status{Code: http.StatusOK}))

	wsContainer.Add(userWS)
	server := &http.Server{Addr: fmt.Sprintf(":%d", port), Handler: wsContainer}
	return server.ListenAndServe()
}

func subDataHandler(request *restful.Request, response *restful.Response) {
	CargoID := request.PathParameter("CargoID")
	Quantity, err := strconv.Atoi(request.QueryParameter("Quantity"))
	if err != nil {
		response.WriteHeaderAndEntity(http.StatusBadRequest,
			status{
				Code:   http.StatusBadRequest,
				Reason: "Quantity参数非法",
			})
		return
	}
	VehicleID := request.PathParameter("VehicleID")
	Bound := request.PathParameter("Bound")
	if Bound != inboundStr && Bound != outboundStr {
		response.WriteHeaderAndEntity(http.StatusBadRequest,
			status{
				Code:   http.StatusBadRequest,
				Reason: "Bound参数非法",
			})
		return
	}
	// TODO handle request
	msg := Message{}
	msg.TimeStamp = time.Now().Unix()
	msg.Records = []Record{Record{ID: CargoID, Quantity: Quantity}}
	err = mqttclient.Publish(msg, Bound, VehicleID)
	if err != nil {
		response.WriteHeaderAndEntity(http.StatusInternalServerError,
			status{
				Code:   http.StatusInternalServerError,
				Reason: "内部错误: " + err.Error(),
			})
		return
	}
	response.WriteEntity(status{Code: http.StatusOK})
}
