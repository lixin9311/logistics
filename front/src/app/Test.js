import React from 'react';
import {Table, TableBody, TableFooter, TableHeader, TableHeaderColumn, TableRow, TableRowColumn}
    from 'material-ui/Table';
import {Tabs, Tab} from 'material-ui/Tabs';
import SwipeableViews from 'react-swipeable-views';
import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';
import {deepOrange500} from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {green100, green500, green700} from 'material-ui/styles/colors';

const muiTheme = getMuiTheme({
    palette: {
        accent1Color: deepOrange500,
    },
}, {
    avatar: {
        borderColor: null,
    },
});
const tablestyles = {
    propContainer: {
        width: 200,
        overflow: 'hidden',
        margin: '20px auto 0',
    },
    propToggleHeader: {
        margin: '20px auto 10px',
    },
};

const tabstyles = {
    headline: {
        fontSize: 24,
        paddingTop: 16,
        marginBottom: 12,
        fontWeight: 400,
    },
    slide: {
        padding: 10,
    },
};

export default class Test extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            slideIndex: 0,
        };
        this.handleChange = (value) => {
            this.setState({
                slideIndex: value,
            });
        };
    }

    render() {
        return (
            <MuiThemeProvider muiTheme={muiTheme}>
            <div>
                <Tabs
                    onChange={this.handleChange}
                    value={this.state.slideIndex}
                >
                    <Tab label="数据" value={0} />
                    <Tab label="控制" value={1} />
                </Tabs>
                <SwipeableViews
                    index={this.state.slideIndex}
                    onChangeIndex={this.handleChange}
                >
                    <div>
                        <DataTable url="http://10.102.11.240:8888/query"/>
                    </div>
                    <div style={tabstyles.slide}>
                        还没做...
                    </div>
                </SwipeableViews>
            </div>
            </MuiThemeProvider>
        );
    }
}

class DataTable extends React.Component {

    constructor(props) {
        super(props);

        this.loadData = () => {
            $.ajax({
                url: this.props.url,
                data: {
                    limit: this.state.page.limit,
                    offset: this.state.page.offset
                },
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    this.setState({data: data.Records});
                    console.log(this.state);
                }.bind(this),
                error: function (xhr, status, err) {
                    console.log(this.props.url, status, err.toString());
                }.bind(this)
            });
        },

        this.state = {
            fixedHeader: true,
            fixedFooter: false,
            stripedRows: false,
            showRowHover: true,
            selectable: true,
            multiSelectable: false,
            enableSelectAll: false,
            deselectOnClickaway: true,
            showCheckboxes: true,
            height: '400px',
            data:[],
            page: {
                limit:20,
                offset:0
            }
        };
        this.handleToggle = (event, toggled) => {
            this.setState({
                [event.target.name]: toggled,
            });
        };

        this.handleChange = (event) => {
            this.setState({height: event.target.value});
        };
    }
    componentDidMount() {
        this.loadData();
    }
    render() {
        return (
            <MuiThemeProvider muiTheme={muiTheme}>
            <div>
                <Table
                    height={this.state.height}
                    fixedHeader={this.state.fixedHeader}
                    fixedFooter={this.state.fixedFooter}
                    selectable={this.state.selectable}
                    multiSelectable={this.state.multiSelectable}
                >
                    <TableHeader
                        displaySelectAll={this.state.showCheckboxes}
                        adjustForCheckbox={this.state.showCheckboxes}
                        enableSelectAll={this.state.enableSelectAll}
                    >
                        <TableRow>
                            <TableHeaderColumn colSpan="3" tooltip="库存记录" style={{textAlign: 'left'}}>
                                库存记录
                            </TableHeaderColumn>
                        </TableRow>
                        <TableRow>
                            <TableHeaderColumn tooltip="记录条目">记录条目</TableHeaderColumn>
                            <TableHeaderColumn tooltip="货物ID">货物ID</TableHeaderColumn>
                            <TableHeaderColumn tooltip="库存">库存</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody
                        displayRowCheckbox={this.state.showCheckboxes}
                        deselectOnClickaway={this.state.deselectOnClickaway}
                        showRowHover={this.state.showRowHover}
                        stripedRows={this.state.stripedRows}
                    >
                        {this.state.data.map( (row, index) => (
                            <TableRow key={index} >
                                <TableRowColumn>{row.Record}</TableRowColumn>
                                <TableRowColumn>{row.ID}</TableRowColumn>
                                <TableRowColumn>{row.Quantity}</TableRowColumn>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </div>
            </MuiThemeProvider>
        );
    }
}