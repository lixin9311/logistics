package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/naoina/toml"
)

var (
	config     = flag.String("c", "", "Config file.")
	role       = ""
	deviceID   = ""
	globalConf = tomlConfig{}
)

// tomlConfig 是配置文件的结构体
type tomlConfig struct {
	Title     string
	Role      string
	DeviceID  string
	Scanner   string
	APIPort   int
	Database  map[string]dbConfig
	MQTT      map[string]mqttConfig
	Mosquitto mosquittoConfig
	Influxdb  influxdConfig
}

// dbConfig 定义了一个数据库的配置
type dbConfig struct {
	// Host 是数据库主机地址，对于MySQL使用[IP]:[Port]，对于Influxdb就是HTTP地址
	Host string
	// User 是数据库用户名
	User string
	// Password 是数据库密码
	Password string
	// DBName 是使用的数据库名
	DBName string
	// Precision 对于Influxdb是记录的精度
	Precision string
}

// mqttConfig 定义了一个mqtt客户端的配置
type mqttConfig struct {
	// Broker 是中间人地址，地址类似于tcp://127.0.0.1:1883
	Broker string
	// RootTopic 是根主题名，注意不要带"/"
	RootTopic string
	// ClientID 是mqtt客户端ID，注意对于同一个broker，不要使用相同的ID
	ClientID string
	// IgnoreError 忽略云端mqtt的错误
	IgnoreError bool
}

// mosquittoConfig 是mosquitto子模块的配置文件
type mosquittoConfig struct {
	// Config 指定了mosquitto使用的配置文件
	Config string
	// Enabled 是否启用，启用之后会导致一堆乱起八糟的输出
	Enabled bool
}

// influxdConfig 是influxdb子模块的配置文件
type influxdConfig struct {
	// Config 指定了influxdb字模块使用的配置文件
	Config string
	// LogFile 指定了influxdb字模块使用的日志文件
	LogFile string
	// Enabled 是否启用，启用之后会导致一堆乱起八糟的输出
	Enabled bool
}

// ToDSN 将数据库的配置转换为DSN格式
func (conf *dbConfig) ToDSN() string {
	return fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8", conf.User, conf.Password, conf.Host, conf.DBName)
}

// ToDSN_NO_DB 将数据库的配置，剔除数据库名字的设置，转换为DSN格式，为了新建数据库用
func (conf *dbConfig) ToDSN_NO_DB() string {
	return fmt.Sprintf("%s:%s@tcp(%s)/?charset=utf8", conf.User, conf.Password, conf.Host)
}

// readConfig 读取配置文件，返回一个配置对象
func readConfig() (result tomlConfig, err error) {
	if *config == "" {
		err = fmt.Errorf("未指定配置文件")
		return
	}

	log.Println("使用指定配置文件: ", *config)
	f, err := os.Open(*config)
	if err != nil {
		err = fmt.Errorf("无法打开配置文件: %v", err)
		return
	}
	defer f.Close()

	buf, err := ioutil.ReadAll(f)
	if err != nil {
		err = fmt.Errorf("无法读取配置文件: %v", err)
		return
	}

	err = toml.Unmarshal(buf, &result)
	if err != nil {
		err = fmt.Errorf("无法解析配置文件: %v", err)
		return
	}
	// TODO 检查配置文件
	switch result.Role {
	case mainStorage:
	case subStorage:
	case vehicle:
	default:
		err = fmt.Errorf("非法Role字段(%s)", result.Role)
	}
	return
}

func main() {
	flag.Parse()
	conf, err := readConfig()
	if err != nil {
		log.Fatalln("读取配置文件出错: ", err)
	}
	globalConf = conf
	role = conf.Role
	deviceID = conf.DeviceID
	// 根据不同的角色开始不同的初始化
	// 四个角色不同的业务分别在main_storage.go，sub_storage.go，
	// vehicle.go，cloud.go里面
	switch role {
	case mainStorage:
		log.Println("节点身份: 主仓库")
		runMainStorage(conf)
	case subStorage:
		log.Println("节点身份: 子仓库")
		runSubStorage(conf)
	case vehicle:
		log.Println("节点身份: 运输车")
		runVehicle(conf)
	case cloud:
		log.Println("节点身份: 云端")
		runCloud(conf)
	}
}
