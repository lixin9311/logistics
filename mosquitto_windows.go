// +build windows

package main

import (
	"fmt"
	"log"
)

// Mosquitto 模块
type Mosquitto struct {
	config string
}

var mosquitto *Mosquitto

func initMosquitto(conf mosquittoConfig) (m *Mosquitto, err error) {
	m = &Mosquitto{conf.Config}
	log.Println("Windows stub.")
	mosquitto = m
	return
}

// Run 启动Mosquitto模块
func (m *Mosquitto) Run() error {
	return fmt.Errorf("Windows stub")
}
