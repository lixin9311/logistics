package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/emicklei/go-restful"
)

func runMainStorage(conf tomlConfig) {
	if conf.Mosquitto.Enabled {
		log.Println("使用内置mosquitto模块.")
		if _, err := initMosquitto(conf.Mosquitto); err != nil {
			log.Fatalln("初始化mosquitto模块出错: ", err)
		}
		go func() {
			err := mosquitto.Run()
			if err != nil {
				log.Fatalln(err)
			}
		}()
	}
	if conf.Influxdb.Enabled {
		log.Println("使用内置Influxdb模块.")
		go func() {
			err := initInfluxd(conf.Influxdb)
			if err != nil {
				log.Fatalln(err)
			}
		}()
	}
	if conf.Mosquitto.Enabled || conf.Influxdb.Enabled {
		time.Sleep(time.Second)
	}

	if _, err := initMysql("default", conf.Database["mysql"]); err != nil {
		log.Fatalln("无法初始化MySQL数据库: ", err)
	}

	if _, err := initInfluxdb(conf.Database["influxdb"]); err != nil {
		log.Fatalln("无法初始化Influxdb数据库: ", err)
	}

	if _, err := initMqtt(conf.MQTT); err != nil {
		log.Fatalln("初始化MQTT客户端出错: ", err)
	}
	if err := mqttclient.Run(); err != nil {
		log.Fatalln("启动MQTT主业务出错: ", err)
	}
	if err := mainHTTPInit(conf.APIPort); err != nil {
		log.Fatalln("启动HTTP接口服务出错: ", err)
	}
}

type singleRecordResponse struct {
	Record StorageStatus
}

type multipleRecordResponse struct {
	Records []StorageStatus
}

func mainHTTPInit(port int) error {
	wsContainer := restful.NewContainer()
	userWS := new(restful.WebService)
	userWS.
		Path("/query").
		Consumes(restful.MIME_JSON).
		Produces(restful.MIME_JSON)

	userWS.Route(userWS.GET("/{CargoID}").
		To(mainSingleHandler).
		Doc("get a access token").
		Operation("getAccessToken").
		Writes(singleRecordResponse{}).
		Param(userWS.PathParameter("CargoID", "CargoID").DataType("string")).
		Returns(200, "OK", singleRecordResponse{}))

	userWS.Route(userWS.GET("/").
		To(mainMultipleHandler).
		Doc("get a access token").
		Operation("getAccessToken").
		Writes(multipleRecordResponse{}).
		Param(userWS.QueryParameter("limit", "limit").DataType("int")).
		Param(userWS.QueryParameter("offset", "offset").DataType("int")).
		Returns(200, "OK", multipleRecordResponse{}))

	wsContainer.Add(userWS)
	server := &http.Server{Addr: fmt.Sprintf(":%d", port), Handler: wsContainer}
	return server.ListenAndServe()
}

func mainSingleHandler(request *restful.Request, response *restful.Response) {
	CargoID := request.PathParameter("CargoID")
	result, err := mysql.GetByCargoID(CargoID)
	if err != nil {
		response.WriteHeaderAndEntity(http.StatusInternalServerError,
			status{
				Code:   http.StatusInternalServerError,
				Reason: "内部错误: " + err.Error(),
			})
		return
	}
	response.AddHeader("Access-Control-Allow-Origin", "*")
	response.WriteEntity(singleRecordResponse{Record: result})
}

func mainMultipleHandler(request *restful.Request, response *restful.Response) {
	limit, err := strconv.Atoi(request.QueryParameter("limit"))
	if err != nil {
		response.WriteHeaderAndEntity(http.StatusBadRequest,
			status{
				Code:   http.StatusBadRequest,
				Reason: "limit参数非法",
			})
		return
	}
	offset, err := strconv.Atoi(request.QueryParameter("offset"))
	if err != nil {
		response.WriteHeaderAndEntity(http.StatusBadRequest,
			status{
				Code:   http.StatusBadRequest,
				Reason: "offset参数非法",
			})
		return
	}

	result, err := mysql.GetAll(limit, offset)
	if err != nil {
		response.WriteHeaderAndEntity(http.StatusInternalServerError,
			status{
				Code:   http.StatusInternalServerError,
				Reason: "内部错误: " + err.Error(),
			})
		return
	}
	response.AddHeader("Access-Control-Allow-Origin", "*")
	response.WriteEntity(multipleRecordResponse{Records: result})
}
