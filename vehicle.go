package main

import "log"

func runVehicle(conf tomlConfig) {
	if _, err := initMysql("default", conf.Database["mysql"]); err != nil {
		log.Fatalln("无法初始化MySQL数据库: ", err)
	}
	if err := startBarcode(conf.Scanner); err != nil {
		log.Fatalln("运行发生错误: ", err)
	}
}
