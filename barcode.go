package main

import (
	"fmt"
	"log"
	"os"
	"time"
)

var boundStatus = inboundStr

// statusChange 定义某个条形码是改变进出方向
var statusChange = "6907992101330"

// startBarcode 是启动二维码扫描的进程
func startBarcode(path string) error {
	fd, err := os.Open(path)
	if err != nil {
		return fmt.Errorf("打开扫码器设备出错: %v", err)
	}
	buf := make([]byte, 128)
	for {
		n, err := fd.Read(buf)
		if err != nil {
			return fmt.Errorf("读取扫码器出错: %v", err)
		}
		// handle
		result := string(buf[:n-1])
		log.Println("扫码结果: ", result)
		if result == statusChange {
			if boundStatus == inboundStr {
				boundStatus = outboundStr
			} else {
				boundStatus = inboundStr
			}
			log.Println("录入状态改变，当前状态: ", boundStatus)
		} else {
			handleCode(result)
		}
	}
}

// handleCode 插入一条二维码数据
func handleCode(code string) {
	msg := Message{}
	msg.Records = []Record{Record{ID: code, Quantity: 1}}
	msg.TimeStamp = time.Now().Unix()
	err := mysql.Insert(msg, boundStatus, "null", "null", deviceID)
	if err != nil {
		log.Println("插入数据出错: ", err)
	}
}
