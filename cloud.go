package main

import (
	"log"
	"time"
)

func runCloud(conf tomlConfig) {

	if _, err := initMysql("default", conf.Database["mysql"]); err != nil {
		log.Fatalln("无法初始化MySQL数据库: ", err)
	}

	if _, err := initInfluxdb(conf.Database["influxdb"]); err != nil {
		log.Fatalln("无法初始化Influxdb数据库: ", err)
	}

	if _, err := initMqtt(conf.MQTT); err != nil {
		log.Fatalln("初始化MQTT客户端出错: ", err)
	}
	if err := mqttclient.Run(); err != nil {
		log.Fatalln("启动MQTT主业务出错: ", err)
	}
	for {
		time.Sleep(time.Hour)
	}
}
