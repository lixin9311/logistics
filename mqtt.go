package main

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/eclipse/paho.mqtt.golang"
)

// mqttClient 是一个mqtt客户端的模型
type mqttClient struct {
	localClient    mqtt.Client
	cloudClient    mqtt.Client
	localRootTopic string
	cloudRootTopic string
	cloudIgnErr    bool
	role           string
	deviceID       string
}

var mqttclient *mqttClient

// mainStorageHandler 处理仓库的进出库请求
var mainStorageHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	rest := strings.Split(msg.Topic(), "/")
	switch len(rest) {
	case 3:
		log.Printf("子仓库(%s)进行操作(%s).\n", rest[1], rest[2])
	case 4:
		vehicleID := rest[2]
		subStorageID := rest[1]
		bound := rest[3]
		log.Printf("车辆(%s)在子仓库(%s)进行操作(%s).\n", vehicleID, subStorageID, bound)
		message := Message{}
		err := json.Unmarshal(msg.Payload(), &message)
		if err != nil {
			log.Println("解析JSON数据出错: ", err)
			return
		}
		// use mysql
		if err = mysql.Insert(message, bound, deviceID, subStorageID, vehicleID); err != nil {
			log.Println("数据插入MySQL数据库出错: ", err)
		}
		// use influxdb
		if err = influxdb.Insert(message, bound, deviceID, subStorageID, vehicleID); err != nil {
			log.Println("数据插入Influxdb数据库出错: ", err)
		}
		// relay
		if err := mqttclient.Publish(msg.Payload(), bound, subStorageID, vehicleID); err != nil {
			log.Println("转发本地数据到云端出错: ", err)
			return
		}
	default:
		log.Println("未解析主题路径: ", msg.Topic())
	}
}

// mainStorageRegHandler 处理子仓库的注册
var mainStorageRegHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	rest := strings.Split(msg.Topic(), "/")
	switch len(rest) {
	case 3:
		log.Printf("子仓库(%s)进行操作(%s).\n", rest[1], rest[2])
	default:
		log.Println("未解析主题路径: ", msg.Topic())
	}
}

// subControlHandler 处理主仓库对子仓库的控制
var subControlHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	// root/sub/control
	rest := strings.Split(msg.Topic(), "/")
	switch len(rest) {
	case 3:
		log.Printf("控制：子仓库(%s)进行操作(%s).\n", rest[1], msg.Payload())
	default:
		log.Println("未解析主题路径: ", msg.Topic())
	}
}

// mainControlHandler 处理云端对仓库的控制
var mainControlHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	// root/main/sub/control
	rest := strings.Split(msg.Topic(), "/")
	switch len(rest) {
	case 4:
		log.Printf("控制：子仓库(%s)进行操作(%s).\n", rest[2], msg.Payload())
		path := globalConf.MQTT["local"].RootTopic + "/" + rest[2] + "/control"
		mqttclient.LocalPub(path, 2, false, msg.Payload())
	case 3:
		log.Printf("控制：主仓库(%s)进行操作(%s).\n", rest[1], msg.Payload())
	default:
		log.Println("未解析主题路径: ", msg.Topic())
	}
}

// cloudHandler 处理主仓库发到云端的数据
var cloudHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	rest := strings.Split(msg.Topic(), "/")
	switch len(rest) {
	case 3:
		log.Printf("主仓库(%s)进行操作(%s).\n", rest[1], rest[2])
	case 5:
		vehicleID := rest[3]
		subStorageID := rest[2]
		mainStorageID := rest[1]
		bound := rest[4]
		log.Printf("车辆(%s)在主仓库(%s)下的子仓库(%s)进行操作(%s).\n", vehicleID, mainStorageID, subStorageID, bound)
		message := Message{}
		err := json.Unmarshal(msg.Payload(), &message)
		if err != nil {
			log.Println("解析JSON数据出错: ", err)
			return
		}
		// use mysql
		if err = mysql.Insert(message, bound, mainStorageID, subStorageID, vehicleID); err != nil {
			log.Println("数据插入MySQL数据库出错: ", err)
		}
		// use influxdb
		if err = influxdb.Insert(message, bound, mainStorageID, subStorageID, vehicleID); err != nil {
			log.Println("数据插入Influxdb数据库出错: ", err)
		}
	default:
		log.Println("未解析主题路径: ", msg.Topic())
	}
}

// cloudRegHandler 处理主仓库的注册
var cloudRegHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	rest := strings.Split(msg.Topic(), "/")
	switch len(rest) {
	case 3:
		log.Printf("主仓库(%s)进行操作(%s).\n", rest[1], rest[2])

	default:
		log.Println("未解析主题路径: ", msg.Topic())
	}
}

var defaultHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	fmt.Printf("TOPIC: %s\n", msg.Topic())
	fmt.Printf("MSG: %s\n", msg.Payload())
}

// 初始化一个mqtt客户端对象
func initMqtt(conf map[string]mqttConfig) (m *mqttClient, err error) {
	m = &mqttClient{}
	m.role = role
	m.deviceID = deviceID
	if role == mainStorage {
		opts := mqtt.NewClientOptions().AddBroker(conf[cloud].Broker)
		opts.SetClientID(conf[cloud].ClientID)
		opts.SetDefaultPublishHandler(defaultHandler)
		opts.SetPingTimeout(1 * time.Second)
		opts.SetKeepAlive(2 * time.Second)
		opts.SetWill(conf[cloud].RootTopic+"/"+m.deviceID+"/lwt", "oops", 2, false)
		m.cloudClient = mqtt.NewClient(opts)
		m.cloudRootTopic = conf[cloud].RootTopic
		m.cloudIgnErr = conf[cloud].IgnoreError
	} else if role == cloud {
		opts := mqtt.NewClientOptions().AddBroker(conf[cloud].Broker)
		opts.SetClientID(conf[cloud].ClientID)
		opts.SetDefaultPublishHandler(defaultHandler)
		opts.SetPingTimeout(1 * time.Second)
		opts.SetKeepAlive(2 * time.Second)
		opts.SetWill(conf[cloud].RootTopic+"/lwt", "oops", 2, false)
		m.cloudClient = mqtt.NewClient(opts)
		m.cloudRootTopic = conf[cloud].RootTopic
		m.cloudIgnErr = conf[cloud].IgnoreError
		mqttclient = m
		return
	}
	opts := mqtt.NewClientOptions().AddBroker(conf["local"].Broker)
	opts.SetClientID(conf["local"].ClientID)
	opts.SetDefaultPublishHandler(defaultHandler)
	opts.SetPingTimeout(10 * time.Second)
	opts.SetKeepAlive(5 * time.Second)
	opts.SetAutoReconnect(true)
	opts.SetMaxReconnectInterval(time.Second)
	opts.SetWill(conf["local"].RootTopic+"/"+m.deviceID+"/lwt", "oops", 2, false)
	m.localClient = mqtt.NewClient(opts)
	m.localRootTopic = conf["local"].RootTopic
	mqttclient = m
	return
}

// Run 跑起来mqtt客户端
func (m *mqttClient) Run() error {

	switch m.role {
	case mainStorage:
		if token := m.localClient.Connect(); token.Wait() && token.Error() != nil {
			err := fmt.Errorf("连接本地MQTT Broker出错: %v", token.Error())
			return err
		}
		// 记录物流数据
		if token := m.localClient.Subscribe(m.localRootTopic+"/+/+/+", 2, mainStorageHandler); token.Wait() && token.Error() != nil {
			err := fmt.Errorf("无法订阅MQTT主题: %v", token.Error())
			return err
		}
		// 记录子仓库状态
		if token := m.localClient.Subscribe(m.localRootTopic+"/+/reg", 2, mainStorageRegHandler); token.Wait() && token.Error() != nil {
			err := fmt.Errorf("无法订阅MQTT主题: %v", token.Error())
			return err
		}
		if token := m.cloudClient.Connect(); token.Wait() && token.Error() != nil {
			err := fmt.Errorf("连接云端MQTT Broker出错: %v", token.Error())
			if m.cloudIgnErr {
				log.Println("已忽略错误: ", err)
			} else {
				return err
			}
		}
		if token := m.cloudClient.Publish(m.cloudRootTopic+"/"+m.deviceID+"/reg", 2, false, "hello"); token.Wait() && token.Error() != nil {
			err := fmt.Errorf("向云端MQTT Broker注册出错: %v", token.Error())
			if m.cloudIgnErr {
				log.Println("已忽略错误: ", err)
			} else {
				return err
			}
		}
		if token := m.cloudClient.Subscribe(m.cloudRootTopic+"/+/control", 2, mainControlHandler); token.Wait() && token.Error() != nil {
			err := fmt.Errorf("无法订阅云端MQTT主题: %v", token.Error())
			return err
		}
		if token := m.cloudClient.Subscribe(m.cloudRootTopic+"/+/+/control", 2, mainControlHandler); token.Wait() && token.Error() != nil {
			err := fmt.Errorf("无法订阅云端MQTT主题: %v", token.Error())
			return err
		}
	case subStorage:
		if token := m.localClient.Connect(); token.Wait() && token.Error() != nil {
			err := fmt.Errorf("连接本地MQTT Broker出错: %v", token.Error())
			return err
		}
		if token := m.localClient.Publish(m.localRootTopic+"/"+m.deviceID+"/reg", 2, false, "hello"); token.Wait() && token.Error() != nil {
			err := fmt.Errorf("向本地MQTT Broker注册出错: %v", token.Error())
			return err
		}
		if token := m.localClient.Subscribe(m.localRootTopic+"/"+m.deviceID+"/control", 2, subControlHandler); token.Wait() && token.Error() != nil {
			err := fmt.Errorf("无法订阅云端MQTT主题: %v", token.Error())
			return err
		}
		return nil
	case cloud:
		if token := m.cloudClient.Connect(); token.Wait() && token.Error() != nil {
			err := fmt.Errorf("连接云端MQTT Broker出错: %v", token.Error())
			if m.cloudIgnErr {
				log.Println("已忽略错误: ", err)
			} else {
				return err
			}
		}
		if token := m.cloudClient.Subscribe(m.cloudRootTopic+"/+/+/+/+", 2, cloudHandler); token.Wait() && token.Error() != nil {
			err := fmt.Errorf("无法订阅云端MQTT主题: %v", token.Error())
			return err
		}
		if token := m.cloudClient.Subscribe(m.cloudRootTopic+"/+/reg", 2, cloudRegHandler); token.Wait() && token.Error() != nil {
			err := fmt.Errorf("无法订阅云端MQTT主题: %v", token.Error())
			return err
		}
	default:
		return nil
	}
	return nil
}

// Publish 发布一条数据消息
// 对于主仓库，IDs参数需要带上子仓库和车的ID
// 对于子仓库，IDs仓鼠需要带上车的ID
func (m *mqttClient) Publish(msg interface{}, bound string, IDs ...string) error {
	if bound != inboundStr && bound != outboundStr {
		return fmt.Errorf("Bound参数错误: %s", bound)
	}
	if m.role == mainStorage {
		if len(IDs) != 2 {
			return fmt.Errorf("非法子仓库ID以及运输车ID: %v", IDs)
		}
		data, err := decode(msg)
		if err != nil {
			return fmt.Errorf("JSON编码时发生错误: %v", err)
		}
		if token := m.cloudClient.Publish(m.cloudRootTopic+"/"+m.deviceID+"/"+IDs[0]+"/"+IDs[1]+"/"+bound, 2, false, data); token.Wait() && token.Error() != nil {
			return fmt.Errorf("向云端MQTT Broker发布消息时出错: %v", token.Error())
		}
	} else {
		if len(IDs) != 1 {
			return fmt.Errorf("非法运输车ID: %v", IDs)
		}
		data, err := decode(msg)
		if err != nil {
			return fmt.Errorf("JSON编码时发生错误: %v", err)
		}
		if token := m.localClient.Publish(m.localRootTopic+"/"+m.deviceID+"/"+IDs[0]+"/"+bound, 2, false, data); token.Wait() && token.Error() != nil {
			return fmt.Errorf("向本地MQTT Broker发布消息时出错: %v", token.Error())
		}
	}
	return nil
}

// LocalPub 向本地发送一条mqtt请求，参数参见mqtt的库
func (m *mqttClient) LocalPub(path string, qos byte, retain bool, data interface{}) error {
	if token := m.localClient.Publish(path, qos, retain, data); token.Wait() {
		return token.Error()
	}
	return nil
}

// CloudPub 向云端发送一条mqtt请求，参数参见mqtt的库
func (m *mqttClient) CloudPub(path string, qos byte, retain bool, data interface{}) error {
	if token := m.cloudClient.Publish(path, qos, retain, data); token.Wait() {
		return token.Error()
	}
	return nil
}

// Close 安全地关闭mqtt客户端
func (m *mqttClient) Close() error {
	m.localClient.Disconnect(250)
	if m.role == mainStorage {
		m.cloudClient.Disconnect(250)
	}
	return nil
}

func decode(msg interface{}) ([]byte, error) {
	switch msg.(type) {
	case string:
		return []byte(msg.(string)), nil
	case []byte:
		return msg.([]byte), nil
	default:
		return json.Marshal(msg.(Message))
	}
}
