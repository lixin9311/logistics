package main

import (
	"database/sql"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql" // import your used driver
)

// MySQL 是数据库对象
type MySQL struct{}

var mysql *MySQL

// InboundRecord 记录模型
type InboundRecord struct {
	ID            int    `orm:"auto"`
	CargoID       string `orm:"size(20)"`
	VehicleID     string `orm:"size(20)"`
	SubStorageID  string `orm:"size(20)"`
	MainStorageID string `orm:"size(20)"`
	Quantity      int
	Timestamp     time.Time
}

// OutboundRecord 记录模型
type OutboundRecord struct {
	ID            int    `orm:"auto"`
	CargoID       string `orm:"size(20)"`
	VehicleID     string `orm:"size(20)"`
	SubStorageID  string `orm:"size(20)"`
	MainStorageID string `orm:"size(20)"`
	Quantity      int
	Timestamp     time.Time
}

// StorageStatus 库存模型
type StorageStatus struct {
	Record   int    `orm:"auto"`
	ID       string `orm:"size(20),unique"`
	Quantity int
}

// TableIndex 返回orm的表索引键
func (s *StorageStatus) TableIndex() [][]string {
	return [][]string{
		[]string{"Record", "ID"},
	}
}

// initMysql 初始化一个MySQL客户端对象
func initMysql(alias string, conf dbConfig) (m *MySQL, err error) {
	m = &MySQL{}
	var db *sql.DB
	orm.RegisterModel(new(InboundRecord))
	orm.RegisterModel(new(OutboundRecord))
	orm.RegisterModel(new(StorageStatus))
	log.Println("使用数据库: ", conf.ToDSN())
	if err = orm.RegisterDataBase(alias, "mysql", conf.ToDSN()); err != nil {
		if strings.Contains(err.Error(), "Unknown database") {
			log.Printf("数据库(%s)不存在, 尝试创建.\n", conf.DBName)
			db, err = sql.Open("mysql", conf.ToDSN_NO_DB())
			if err != nil {
				err = fmt.Errorf("无法打开数据库: %v", err)
				return
			}

			if _, err = db.Exec("CREATE DATABASE " + conf.DBName); err != nil {
				err = fmt.Errorf("无法创建数据库: %v", err)
				return
			}

			if err = orm.RegisterDataBase("default", "mysql", conf.ToDSN()); err != nil {
				err = fmt.Errorf("无法打开数据库: %v", err)
				return
			}
		} else {
			err = fmt.Errorf("无法打开数据库: %v", err)
			return
		}
	}
	err = orm.RunSyncdb("default", false, true)
	if err != nil {
		err = fmt.Errorf("无法同步ORM模型: %v", err)
		return
	}
	mysql = m
	return
}

// Insert 插入一条记录到MySQL数据库中
func (m *MySQL) Insert(msg Message, bound string, IDs ...string) error {
	if len(IDs) != 3 {
		return fmt.Errorf("非法ID参数: %v", IDs)
	}
	o := orm.NewOrm()
	err := o.Begin()
	if err != nil {
		return fmt.Errorf("开始MySQL业务时出错: %v", err)
	}

	var ts time.Time
	if msg.TimeStamp == 0 {
		ts = time.Now()
	} else {
		ts = time.Unix(msg.TimeStamp, 0)
	}
	storageStatus := map[string]StorageStatus{}
	if bound == inboundStr {
		for _, v := range msg.Records {
			record := new(InboundRecord)
			record.CargoID = v.ID
			record.Quantity = v.Quantity
			record.MainStorageID = IDs[0]
			record.SubStorageID = IDs[1]
			record.VehicleID = IDs[2]
			record.Timestamp = ts
			_, err = o.Insert(record)
			if err != nil {
				goto err
			}
			if _, ok := storageStatus[v.ID]; !ok {
				tmp := StorageStatus{ID: v.ID}
				if _, _, err = o.ReadOrCreate(&tmp, "ID"); err != nil {
					goto err
				} else {

					tmp.Quantity += v.Quantity
					storageStatus[v.ID] = tmp
				}
			} else {
				tmp := storageStatus[v.ID]
				tmp.Quantity += v.Quantity
				storageStatus[v.ID] = tmp
			}
		}
	} else if bound == outboundStr {
		for _, v := range msg.Records {
			record := new(OutboundRecord)
			record.CargoID = v.ID
			record.Quantity = v.Quantity
			record.MainStorageID = IDs[0]
			record.SubStorageID = IDs[1]
			record.VehicleID = IDs[2]
			record.Timestamp = ts
			_, err = o.Insert(record)
			if err != nil {
				goto err
			}
			if _, ok := storageStatus[v.ID]; !ok {
				tmp := StorageStatus{ID: v.ID}
				if _, _, err = o.ReadOrCreate(&tmp, "ID"); err != nil {
					goto err
				} else {
					tmp.Quantity -= v.Quantity
					if tmp.Quantity < 0 {
						log.Println("库存数量小于0!")
						tmp.Quantity = 0
					}
					storageStatus[v.ID] = tmp
				}
			} else {
				tmp := storageStatus[v.ID]
				tmp.Quantity -= v.Quantity
				if tmp.Quantity < 0 {
					log.Println("库存数量小于0!")
					tmp.Quantity = 0
				}
				storageStatus[v.ID] = tmp
			}
		}
	}
	for _, v := range storageStatus {
		if _, err = o.Update(&v); err != nil {
			goto err
		}
	}
	err = o.Commit()
	if err != nil {
		err = fmt.Errorf("提交数据库操作出错: %v", err)
		goto err
	}
	return nil
err:
	log.Println("插入记录出错: ", err)
	log.Println("尝试回滚操作...")
	err2 := o.Rollback()
	if err2 != nil {
		err = fmt.Errorf("回滚数据出错: %v", err2)
	}
	return err
}

// GetByCargoID 通过货物ID查询MySQL库存记录
func (m *MySQL) GetByCargoID(id string) (StorageStatus, error) {
	var record StorageStatus
	o := orm.NewOrm()
	qs := o.QueryTable(new(StorageStatus))
	err := qs.Filter("ID__exact", id).One(&record)
	// record := StorageStatus{ID: id}
	// err := o.Read(&record, "ID")
	return record, err
}

// GetAll 获取全部库存记录
func (m *MySQL) GetAll(limit, offset int) ([]StorageStatus, error) {
	var records []StorageStatus
	o := orm.NewOrm()
	qs := o.QueryTable(new(StorageStatus))
	_, err := qs.Limit(limit).Offset(offset).All(&records)
	return records, err
}
