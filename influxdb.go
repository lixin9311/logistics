package main

import (
	"fmt"
	"time"

	"github.com/influxdata/influxdb/client/v2"
)

// Influxdb 数据库客户端
type Influxdb struct {
	c         client.Client
	precision string
	dbname    string
}

var influxdb *Influxdb

// initInfluxdb 初始化influxdb客户端
func initInfluxdb(conf dbConfig) (i *Influxdb, err error) {
	i = &Influxdb{}
	i.c, err = client.NewHTTPClient(client.HTTPConfig{
		Addr:     conf.Host,
		Username: conf.User,
		Password: conf.Password,
	})
	if err != nil {
		return
	}
	i.precision = conf.Precision
	i.dbname = conf.DBName
	influxdb = i
	q := client.NewQuery("CREATE DATABASE "+i.dbname, "", "")
	res, err := i.c.Query(q)
	if err != nil {
		err = fmt.Errorf("新建数据库出错: %v", err)
		return
	}
	if res.Error() != nil {
		err = fmt.Errorf("新建数据库返回错误: %v", res.Error())
		return
	}
	return
}

// Insert 插入一组数据到Inluxdb中
func (i *Influxdb) Insert(msg Message, bound string, IDs ...string) error {
	if len(IDs) != 3 {
		return fmt.Errorf("非法ID参数: %v", IDs)
	}
	bp, err := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  i.dbname,
		Precision: i.precision,
	})
	if err != nil {
		return fmt.Errorf("新建数据集合出错: %v", err)
	}
	var ts time.Time
	if msg.TimeStamp == 0 {
		ts = time.Now()
	} else {
		ts = time.Unix(msg.TimeStamp, 0)
	}
	for _, v := range msg.Records {
		tags := map[string]string{
			"MainStorageID": IDs[0],
			"SubStorageID":  IDs[1],
			"VehicleID":     IDs[2],
			"Bound":         bound,
			"CargoID":       v.ID,
		}
		fields := map[string]interface{}{
			"Quantity": v.Quantity,
		}
		pt, err := client.NewPoint(inboundStr, tags, fields, ts)
		if err != nil {
			return fmt.Errorf("新建数据点出错: %v", err)
		}
		bp.AddPoint(pt)
	}
	return i.c.Write(bp)
}
