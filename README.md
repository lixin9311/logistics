# 四种角色定义

| 角色           | 功能模块组成    |
| :------------- | :------------- |
| vehicle        | 条码扫描、UWB驱动、MySQL客户端、服务器|
| sub_storage    | mqtt客户端、HTTP接口、UWB驱动|
| main_storage   | mqtt客户端、服务器、influxdb客户端、<br>服务器、MySQL客户端、服务器、HTTP接口|
| cloud          | mqtt客户端、服务器、influxdb客户端、<br>服务器、MySQL客户端、服务器、HTTP接口|


### vehicle

定义为运输车，负责在仓库之间运输货物。
车载需要MySQL数据库保存车载货物内容。
同时需要条码扫描器扫描登记货物信息。

在抵达仓库时，通过UWB模块与仓库测距通信，将运载内容通知仓库。

### sub_storage

定义为子仓库，是主仓库的下属，负责直接处理运输车的请求。
通过UWB模块与运输车通信，得到运输车的内容。
UWB驱动通过HTTP接口向主要服务通知运输内容，之后进入主服务处理流程。

HTTP接口负责接收运输内容的参数：

###### GET /data/{VehicleID}/{Bound}/{CargoID}?Quantity=

说明：负责接收出入库数据，然后转为mqtt报文上报主仓库。
返回处理结果。

| 参数           | 说明                              |
| :------------- | :------------------------------- |
| {VehicleID}    | 运输车ID                          |
| {Bound}        | inbound、outbound 分别是进库和出库 |
| {CargoID}      | 货物ID                            |
| Quantity       | 货物数量                          |

例子：

GET /data/vehicle_123/inbound/cargo_456?Quantity=10

返回：

```json
{
  "Code" : 400,
  "Reason" : "非法货物数量"
}
```

### main_storage

定义为主仓库，负责处理子仓库的请求，并向云端汇报，同时负责云端的控制请求，向下级转发。
他将接收到的货物出入库数据同时记录在Influxdb和MySQL数据库中。
其中MySQL使用了三张表：

| 表名            | 对应对象          | 说明     |
| :-------------- | :--------------- | :------ |
| inbound_record  | InboundRecord    | 入库日志 |
| outbound_record | OutboundRecord   | 出库日志 |
| storage_status  | StorageStatus    | 库存状态 |

Influxdb记录的内容是出入库记录日志，维度信息和MySQL相同，分别是：

1. 车辆ID
2. 子仓库ID
3. 主仓库ID
4. 货物ID
5. 货物数量
6. 时间戳

同时存在HTTP接口：

###### GET /query?limit=&offset=

说明：查询全部库存记录

| 参数     | 说明             |
| :------- | :-------------- |
| limit    | 查询结果数量限制 |
| offset   | 查询偏移量       |

例子：

GET /query?limit=20&offset=40

返回：
```json
{
  "Records" : [
    {
      "ID" : "cargo_456",
      "Quantity" : 20
    },
    {
      "ID" : "cargo_123",
      "Quantity" : 10
    }
  ]
}
```

###### GET /query/{CargoID}

说明：根据货物ID查询库存记录

| 参数      | 说明     |
| :-------- | :------ |
| {CargoID} | 货物ID  |

例子：

GET /query/cargo_456

返回：
```json
{
  "Record" : {
      "ID" : "cargo_456",
      "Quantity" : 20
    }
}
```

### cloud

云端目前和主仓库功能一致，只是将收集到的数据做了聚合。

---

# 业务流程

### 入库

指车辆进入仓库卸载货物。

1. 首先车辆通过UWB模块与子仓库进行交互，将货物内容通知子仓库。
2. 子仓库的UWB驱动得到了货物内容后，调用HTTP接口，将货物内容转换为mqtt报文，上报给主仓库
3. 主仓库记录操作记录到日志中，然后上报给云端
4. 云端记录操作日志

### MQTT 主题划分

对于主仓库，上面有三种主题：

1. {RootTopic}/{SubStorageID}/{VehicleID}/{Bound}
  * 这里发布着不同车辆的进出库操作，由Bound区分进出
  * 报文见protocol.go
2. {RootTopic}/{SubStorageID}/control
  * 这里是对子仓库的控制
3. {RootTopic}/{SubStorageID}/reg
  * 这里是子仓库的注册

进出库数据报文如下：

```json
{
  "Record" : [
    {
      "ID" : "cargo_456",
      "Quantity" : 20
    },
    {
      "ID" : "cargo_123",
      "Quantity" : 20
    }
  ],
  "TimeStamp" : 13214,
  "Extension" : null
}
```

对于云端，上面是四种主题：

1. {RootTopic}/{MainStorageID}/{SubStorageID}/{VehicleID}/{Bound}
  * 这里发布着不同车辆的进出库操作，由Bound区分进出
  * 报文见protocol.go
2. {RootTopic}/{MainStorageID}/control
  * 这里是对主仓库的控制
3. {RootTopic}/{MainStorageID}/{SubStorageID}/control
  * 这里是对子仓库的控制
4. {RootTopic}/{MainStorageID}//reg
  * 这里是主仓库的注册
